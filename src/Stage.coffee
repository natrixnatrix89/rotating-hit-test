MovingInstance = require('./MovingInstance.coffee')
Vector2D = require('./Vector2D.coffee')
raf = require('./requestAnimationFrame.js')

module.exports = class Stage
  constructor: (@canvas)->
    @canvas.width = @canvas.parentNode.offsetWidth
    @canvas.height = @canvas.parentNode.offsetHeight
    @ctx = @canvas.getContext "2d"

    if not @ctx?
      throw new Error "Could not access the 2d context of canvas"

    @instances = []
    @addInstances()
    @tick = @tick.bind @
    @canvas.addEventListener 'click', (e)=>
      @interaction(e.pageX, e.pageY)
    @canvas.addEventListener 'touchstart', (e)=>
      @interaction(e.touches[0].pageX, e.touches[0].pageY)
    @canvas.addEventListener 'touchend', (e)->
      e.preventDefault()
    @tick()

  addInstances: ->
    rows = 20
    columns = 20
    xspacing = @canvas.width / (columns + 1)
    yspacing = @canvas.height / (rows + 1)
    minSpacing = Math.min(xspacing, yspacing)
    for k in [1..columns]
      x = k * xspacing
      for l in [1..rows]
        y = l * yspacing
        i = new MovingInstance(new Vector2D(x, y), minSpacing * 0.15, minSpacing * 0.4)
        @instances.push i

  tick: ->
    for instance in @instances
      instance.tick()
      instance.paint(@ctx)

    raf.requestAnimationFrame.call window, @tick

  interaction: (x, y)->
    v = new Vector2D(x, y)
    if instance = @hitTest v
      instance.action()

  hitTest: (v)->
    for instance in @instances
      if instance.hitTest v
        return instance
    return false