randomColor = require('randomcolor')
Vector2D = require('./Vector2D.coffee')

pi2 = Math.PI * 2

module.exports = class MovingInstance
  rotatingClockWise: yes
  rotationSpeed: 0
  maxRotationSpeed: Math.PI / 60 # assuming 60 FPS -> half resolution per second
  rotationSpeedStep: Math.PI / 1000
  constructor: (@center, @innerRadius = 5, @outerRadius = 15)->
    @openSector = (Math.PI / 2) + Math.random() * Math.PI # somewhere between 1/4 and 3/4
    if not (@center instanceof Vector2D)
      throw new Error "Position of an instance must be passed as a 2d Vector"
    @rotation = 0
    @color = randomColor
      luminosity: 'light'
    @clearArgs = [
      @center.v[0] - @outerRadius - 1
      @center.v[1] - @outerRadius - 1
      @outerRadius * 2 + 2
      @outerRadius * 2 + 2
    ]
    @bounds = [
      @center.v[0] - @outerRadius - 1
      @center.v[1] - @outerRadius - 1
      @center.v[0] + @outerRadius + 1
      @center.v[1] + @outerRadius + 1
    ]

  paint: (ctx)->
    @clear ctx
    ctx.save()

    startAngle = @rotation + @openSector
    endAngle = @rotation

    ctx.fillStyle = @color
    ctx.beginPath()
    ctx.arc @center.v[0], @center.v[1], @outerRadius, startAngle, endAngle, false
    ctx.arc @center.v[0], @center.v[1], @innerRadius, endAngle, startAngle, true
    ctx.fill()

    ctx.restore()

  clear: (ctx)->
    ctx.clearRect @clearArgs...

  tick: ->
    if @rotatingClockWise
      if @rotationSpeed < @maxRotationSpeed
        @rotationSpeed += @rotationSpeedStep
    else
      if @rotationSpeed > -@maxRotationSpeed
        @rotationSpeed -= @rotationSpeedStep

    @rotation += @rotationSpeed

    if @rotation >= pi2
      @rotation %= pi2

    if @rotation < 0
      if @rotation < -pi2
        @rotation %= pi2
      @rotation += pi2

  hitTest: (t)->
    if not (t instanceof Vector2D)
      throw new Error "wrong argument type"
    # if the point is outside the bounds don't proceed with any trig
    if not (@bounds[0] < t.v[0] < @bounds[2])
      return false
    if not (@bounds[1] < t.v[1] < @bounds[3])
      return false

    relativeVector = t.subtractVector @center
    distance = relativeVector.getDistance()

    if distance > @outerRadius
      return false

    if distance < @innerRadius
      return false

    vDirection = relativeVector.getDirection()
    startAngle = @rotation + @openSector
    startAngle %= pi2
    endAngle = @rotation

    if startAngle < endAngle
      return startAngle <= vDirection <= endAngle
    else
      return (startAngle <= vDirection) or (vDirection <= endAngle)

  action: ->
    @rotatingClockWise = not @rotatingClockWise
    @color = randomColor()
