var lastTime = 0;
var vendors = ['webkit', 'moz'];
exports.requestAnimationFrame = window.requestAnimationFrame;
exports.cancelAnimationFrame = window.cancelAnimationFrame;
for(var x = 0; x < vendors.length && !exports.requestAnimationFrame; ++x) {
    exports.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
    exports.cancelAnimationFrame =
      window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
}

if (!exports.requestAnimationFrame)
    exports.requestAnimationFrame = function(callback, element) {
        var currTime = new Date().getTime();
        var timeToCall = Math.max(0, 16 - (currTime - lastTime));
        var id = window.setTimeout(function() { callback(currTime + timeToCall); },
          timeToCall);
        lastTime = currTime + timeToCall;
        return id;
    };

if (!exports.cancelAnimationFrame)
    exports.cancelAnimationFrame = function(id) {
        clearTimeout(id);
    };
