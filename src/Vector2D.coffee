pi2 = Math.PI * 2

module.exports = class Vector2D
	constructor: (@v...)->
    if @v.length isnt 2
      throw new Error "Invalid count of arguments for 2d vector given. 2 expected."
    for val in @v
      if typeof val isnt 'number'
        throw new Error "Vector2D accepts only numbers as arguments."

  multiplyScalar: (scalar)->
    out = new Vector2D 0, 0
    for val, id in @v
      out.v[id] = val * scalar
    out

  addVector: (vector)->
    if not (vector instanceof Vector2D)
      throw new Error "Invalid argument for addVector. A type of Vector2D expected"

    out = new Vector2D 0, 0
    for val, id in @v
      out.v[id] = val + vector.v[id]

    out

  subtractVector: (vector)->
    if not (vector instanceof Vector2D)
      throw new Error "Invalid argument for subtractVector. A type of Vector2D expected"

    out = new Vector2D 0, 0
    for val, id in @v
      out.v[id] = val - vector.v[id]

    out

  getDistance: ->
    Math.sqrt @v[0] * @v[0] + @v[1] * @v[1]

  isClockwise: (vector)->
    @v[0] * vector.v[1] + @v[1] * vector.v[0] > 0

  getDirection: ->
    d = Math.atan2 @v[1], @v[0]
    if d < 0
      d += pi2
    d
  getValues: -> @v[..]
